open Lwt
open Cohttp
open Cohttp_lwt_unix

module type PropertyType = sig 
    val server_key : string
end

module type M = sig 
 val post_message : unit -> unit Lwt.t
end

module Make (M : PropertyType) : M = struct

    let fcm_send_uri = Uri.of_string "https://fcm.googleapis.com/fcm/send"

    let headers = 
        let authorization_header_key = Printf.sprintf "key=%s" M.server_key in
        let header_list = [("Content-Type", "application/json"); ("Authorization", authorization_header_key)] in 
        Cohttp.Header.add_list (Cohttp.Header.init ()) header_list

    let body = 
        let message = Fcm_t.{
            topic = "/topics/vibration_small";
            data = {
                token = "token"
            }
        } in
        message |> Fcm_j.string_of_message |> Cohttp_lwt.Body.of_string

    let post_message () =
        Client.post fcm_send_uri ~body ~headers 
        >>= fun (resp, body) -> body |> Cohttp_lwt.Body.to_string 
        >|= fun body -> Lwt_io.printf "Body: %s\n" body |> ignore

end